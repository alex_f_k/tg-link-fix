<!-- TOC -->
* [Fix link bot](#fix-link-bot)
    * [Working:](#working)
    * [To add:](#to-add)
* [Setup](#setup)
  * [venv](#venv)
  * [Token](#token)
  * [systemd (optional)](#systemd-optional)
<!-- TOC -->

# Fix link bot
Simple inline [telegram bot](https://t.me/fixlinbot) that swaps social media links with preview enabled ones.

### Working:
* twitter.com -> fxtwitter.com
  * x.com -> fxtwitter.com
* instagram.com -> ddinstagram.com
* tiktok.com -> vxtiktok.com

### To add:
* reddit.com -> rxddit.com

# Setup
While you can rawdog it with `nohup python main.py &> run.log &`,
I would recommend this simple setup.

## venv
```shell
python3 -m venv .venv

. .venv/bin/activate

pip install -r requirements.txt
```

## Token
I expect `token.txt` file in the same folder. It would contain
tg bot token (duh).

> Without this file the script will fail
> with `FileNotFoundError` exception on start.

## systemd (optional)
To survive a reboot I have [py-fixlinbot.service](py-fixlinbot.service) file.
To use it:
1) Create separate group
    ```shell
    groupadd py-fixlingroup
    usermod -aG py-fixlingroup <your-user>
    ```
2) Setup env config pointing to the working directory
    ```shell
    mkdir /etc/py-fixlinbot/
    echo "PROJ_DIR=/your/folder" > /etc/py-fixlinbot/conf.env
    ```
3) Setup polkit to control this service without sudo
    ```js
    /**
      /etc/polkit-1/rules.d/50-systemd-py-group.rules
    */
    polkit.addRule(function(action, subject) {
        if (action.id == "org.freedesktop.systemd1.manage-units" &&
            action.lookup("unit") == "py-fixlinbot.service"  &&
            subject.isInGroup("py-fixlingroup")
        ) {
            return polkit.Result.YES;
        }
    })
    ```
4) Copy or create placeholder for systemd service with rw rights for the group
    ```shell
    sudo chown <your-user>:py-fixlingroup /usr/lib/systemd/system/py-fixlinbot.service
    sudo chmod 664 /usr/lib/systemd/system/py-fixlinbot.service
    ```
