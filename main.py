import logging
import re

from telegram import Update, InlineQueryResultArticle, InputTextMessageContent, LinkPreviewOptions
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, InlineQueryHandler, AIORateLimiter

# Enable logging
logging.basicConfig(
    filename="run.log",
    format="%(asctime)s [%(levelname)s] %(name)s : %(message)s",
    level=logging.INFO,
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

application = {}

# https://twitter.com/thegrandtour/status/1753470842141323567
# https://x.com/i/status/1753470842141323567?s=46&t=1uVjrgoIumv3rxErqQzU0A
twitter_full_match = re.compile(r"^(https?://)?(x|twitter)\.com/"
                                r"([a-zA-Z0-9_]{1,15}"
                                r"/status"
                                r"/\d+)")
twitter_domain_match = re.compile(r"^(https?://)?(x|twitter)\.com")

# GET /p/Cw8X2wXPjiM
# GET /stories/fatfatpankocat/3226148677671954631/
insta_full_match = re.compile(r"^(https?://)?(www\.)?(instagram)\.com/"
                              r"([a-zA-Z0-9\-_=/]+)")
insta_domain_match = re.compile(r"^(https?://)?(www\.)?(instagram)\.com")

# https://vt.tiktok.com/ZSF8JGc2H/
# https://www.tiktok.com/t/ZT8WJh4Sf/
# https://www.tiktok.com/@lyssielooloo/video/7317786992962719022
tiktok_full_match = re.compile(r"^(https?://)?((vt|www)?\.?tiktok)\.com/"
                               r"([@a-zA-Z0-9\-_=/]+)")
tiktok_domain_match = re.compile(r"^(https?://)?(www|vt)?\.?(tiktok)\.com/")

g_link_preview_options = LinkPreviewOptions(
    prefer_large_media=True,
    show_above_text=True,
    is_disabled=False,
)
"""Global link preview setting. Always shows media, enlarges it and places text at the bottom of the message"""


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text(text="Hi, this is an inline bot\. Type `@fixlinbot` in any chat and follow with "
                                         "the social media link",
                                    parse_mode="MarkdownV2")


async def inline_twitter(update: Update, context: ContextTypes.DEFAULT_TYPE):
    query = update.inline_query.query

    if not query:  # empty query should not be handled
        return

    match = re.match(twitter_full_match, query)
    site = match.group(2)

    new_link = "https://fxtwitter.com/{}".format(match.group(3))
    new_en_link = "https://fxtwitter.com/{}/en".format(match.group(3))
    new_ru_link = "https://fxtwitter.com/{}/ru".format(match.group(3))
    await update.inline_query.answer(
        results=[
            # plain twitter embed
            InlineQueryResultArticle(
                id=str(update.inline_query.id) + "0",
                title=("{}.com -> fxtwitter.com".format(site)),
                input_message_content=InputTextMessageContent(
                    message_text=new_link,
                    link_preview_options=g_link_preview_options,
                ),
                url=new_link
            ),
            # embed with any lang -> en translation
            InlineQueryResultArticle(
                id=str(update.inline_query.id) + "1",
                title=("{}.com -> fxtwitter.com 🇬🇧".format(site)),
                input_message_content=InputTextMessageContent(
                    message_text=new_en_link,
                    link_preview_options=g_link_preview_options,
                ),
                url=new_en_link
            ),
            # embed with any lang -> ru translation
            InlineQueryResultArticle(
                id=str(update.inline_query.id) + "2",
                title=("{}.com -> fxtwitter.com 🇷🇺".format(site)),
                input_message_content=InputTextMessageContent(
                    message_text=new_ru_link,
                    link_preview_options=g_link_preview_options,
                ),
                url=new_ru_link
            )
        ]
    )


async def inline_insta(update: Update, context: ContextTypes.DEFAULT_TYPE):
    query = update.inline_query.query
    match = re.match(insta_full_match, query)

    new_link = "https://ddinstagram.com/{}".format(match.group(4))
    await update.inline_query.answer(
        results=[
            InlineQueryResultArticle(
                id=str(update.inline_query.id) + "0",
                title="instagram -> ddinstagram",
                input_message_content=InputTextMessageContent(
                    message_text=new_link,
                    link_preview_options=g_link_preview_options,
                ),
                url=new_link
            )
        ]
    )


async def inline_tiktok(update: Update, context: ContextTypes.DEFAULT_TYPE):
    query = update.inline_query.query
    match = re.match(tiktok_full_match, query)

    site = match.group(2)
    new_site = site.replace("tiktok", "vxtiktok")
    new_link = "https://{site}.com/{post}".format(site=new_site, post=match.group(4))
    await update.inline_query.answer(
        results=[
            InlineQueryResultArticle(
                id=str(update.inline_query.id) + "0",
                title="{site} -> {new_site}".format(site=site, new_site=new_site),
                input_message_content=InputTextMessageContent(
                    message_text=new_link,
                    link_preview_options=g_link_preview_options,
                ),
                url=new_link
            )
        ]
    )


async def error_handler(update: object, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Log the error"""
    # Log the error before we do anything else, so we can see it even if something breaks.
    logger.error("Exception while handling an update:", exc_info=context.error)


def main():
    with open("token.txt", "r") as f:
        token = f.readline().strip()

    global application
    application = (ApplicationBuilder().token(token)
                   .rate_limiter(AIORateLimiter())
                   .build())
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application.add_handler(CommandHandler('start', start))
    # Register inline replies ...
    application.add_handler(InlineQueryHandler(inline_twitter, twitter_domain_match))
    application.add_handler(InlineQueryHandler(inline_insta, insta_domain_match))
    application.add_handler(InlineQueryHandler(inline_tiktok, tiktok_domain_match))
    # ... and the error handler
    application.add_error_handler(error_handler)
    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=[Update.INLINE_QUERY, Update.MESSAGE])


if __name__ == "__main__":
    main()
