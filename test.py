import re
from unittest import TestCase

from main import insta_full_match, tiktok_full_match, twitter_full_match


class TestClass(TestCase):
    def test_twitter(self):
        match = re.match(twitter_full_match, "twitter.com/thegrandtour/status/1753470842141323567")
        assert match.group(3) == "thegrandtour/status/1753470842141323567"

        match = re.match(twitter_full_match, "https://x.com/i/status/1753470842141323567?s=46&t=1uVjrgoIumv3rxErqQzU0A")
        assert match.group(3) == "i/status/1753470842141323567"

    def test_insta(self):
        match = re.match(insta_full_match, "https://www.instagram.com/p/Cw8X2wXPjiM")
        assert match.group(4) == "p/Cw8X2wXPjiM"
        match = re.match(insta_full_match, "instagram.com/stories/fatfatpankocat/3226148677671954631/")
        assert match.group(4) == "stories/fatfatpankocat/3226148677671954631/"

    def test_tiktok(self):
        match = re.match(tiktok_full_match, "https://vt.tiktok.com/ZSF8JGc2H/")
        assert match.group(4) == "ZSF8JGc2H/"

        match = re.match(tiktok_full_match, "www.tiktok.com/t/ZT8WJh4Sf/")
        assert match.group(4) == "t/ZT8WJh4Sf/"

        match = re.match(tiktok_full_match, "https://tiktok.com/@lyssielooloo/video/7317786992962719022")
        assert match.group(4) == "@lyssielooloo/video/7317786992962719022"
