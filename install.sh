#!/bin/zsh

# Exit on failure
set -e
# Print commands
set -x

# Fail if no config file present
if [[ ! -f "/etc/py-fixlinbot/conf.env" ]]; then
  echo "Create systemd service env file at"
  echo "    /etc/py-fixlinbot/conf.env"
  return 1
fi

# Checkout latest version
git pull

# Install venv if doesn't exist
if [[ ! -d ".venv" ]]; then
  python3.11 -m venv .venv
fi

# Install/update requirements
./.venv/bin/pip install -r requirements.txt

# Install service
cp -v py-fixlinbot.service /usr/lib/systemd/system/

echo
echo "'systemctl daemon-reload' might be required if service file has changed"
echo
# enable and restart
#systemctl enable py-fixlinbot.service
systemctl restart py-fixlinbot.service
